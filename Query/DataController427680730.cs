using IPS.DataGovernance.Microservice.API.Query;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace IPS.DataGovernance.Microservice.API
{
    public class DataController
    {
        private readonly IMediator _mediator;
        private readonly ILogger _logger;
        public DataController(IMediator mediator, ILogger<DataController>logger)
        {            
            _mediator = mediator;
            _logger = logger;
        }

        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [FunctionName("query")]
        public async Task<IActionResult> CreateDataQuery(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post")]DataQuery dataQuery)
        {   
            var response = await _mediator.TrySend(dataQuery);
            if(response.IsValid)
                return new OkObjectResult(response.Result);
            return new BadRequestObjectResult(response.ValidationErrors);            
        }
    }
}
